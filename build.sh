#!/bin/sh

# Usage: build.sh [cargo_args]+
#
# The purpose of this script is to handle the case where gcc is compiled with
# --enable-default-pie and rustc doesn't have the fix to pass -no-pie when
# needed.

cc -v 2>&1 | grep enable-default-pie 1>/dev/null

if [ $? -eq 0 ];then
    RUSTFLAGS="-C link-arg=-no-pie $RUSTFLAGS" xargo build "$@"
else
    xargo build "$@"
fi
